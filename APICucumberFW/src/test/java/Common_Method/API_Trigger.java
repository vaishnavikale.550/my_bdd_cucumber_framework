package Common_Method;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {

	public static Response Post_trigger(String HeaderName, String HeaderValue, String Reqbody, String Endpoint) {

		// Build the request specification using RequestSpecification class

		RequestSpecification requestSpec = RestAssured.given();

		// Set request header

		requestSpec.header(HeaderName, HeaderValue);

		// Set request body

		requestSpec.body(Reqbody);

		// Send the API request

		Response response = requestSpec.post(Endpoint);
		return response;
	}

	public static Response Put_trigger(String HeaderName, String HeaderValue, String Reqbody, String Endpoint) {

		RequestSpecification requestSpec = RestAssured.given();
		requestSpec.header(HeaderName, HeaderValue);
		requestSpec.body(Reqbody);
		Response response = requestSpec.put(Endpoint);
		return response;
	}

	public static Response Patch_trigger(String HeaderName, String HeaderValue, String Reqbody, String Endpoint) {

		RequestSpecification requestSpec = RestAssured.given();

		requestSpec.header(HeaderName, HeaderValue);

		requestSpec.body(Reqbody);

		Response response = requestSpec.patch(Endpoint);
		return response;
	}
	public static Response Get_trigger(String HeaderName, String HeaderValue, String Reqbody, String Endpoint) {

		RequestSpecification requestSpec = RestAssured.given();

		requestSpec.header(HeaderName, HeaderValue);

		Response response = requestSpec.get(Endpoint);
		return response;
	}
}
