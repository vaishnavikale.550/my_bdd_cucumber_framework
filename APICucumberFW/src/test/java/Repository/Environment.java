package Repository;

public class Environment {

	public static String Hostname() {
		String hostname = "https://reqres.in/";
		return hostname;
	}

	public static String Resource_Post_API() {
		String resource_Post_API = "api/users";
		return resource_Post_API;
	}

	public static String HeaderName() {

		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {

		String headervalue = "application/json";
		return headervalue;

	}

	public static String Resource_Patch_API() {
		String resource_Patch_API = "api/users/2";
		return resource_Patch_API;
	}

	public static String Resource_Delete_API() {
		String resource_Delete_API = "api/users/2";
		return resource_Delete_API;
	}

	public static String Resource_Put_API() {
		String resource_Put_API = "api/users/2";
		return resource_Put_API;
	}

	public static String Resource_Get_API_ListUser() {
		String resource_Get_API = "api/users?page=2";
		return resource_Get_API;
	}
}
