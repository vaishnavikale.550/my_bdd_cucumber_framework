package Repository;

import java.util.ArrayList;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import Common_Method.Utility;

public class RequestBody extends Environment {

	public static String req_post_tc() throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Post_API", "Post_TC1");
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);

		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";
		return req_body;

	}

	public static String req_put_tc() throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Put_API", "Put_TC1");
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);

		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";
		return req_body;

	}

	public static String req_Patch_tc() throws IOException {
		ArrayList<String> Data = Utility.readExcelData("Patch_API", "Patch_TC1");
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);

		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";
		return req_body;
	}

	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] { { "morpheus", "leader" }, { "vaishnavi", "qa" } };
	}

}


