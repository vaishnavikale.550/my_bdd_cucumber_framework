package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Method.API_Trigger;
import Common_Method.Utility;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PatchStepDefinition {
	File dir_name;
	String requestBody;
	String Endpoint;
	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_updatedAt;
	
	@Before("@Patch_API_TC")
    public void beforeScenariopatch() throws IOException{
		dir_name = Utility.CreateLogDirectory("Patch_API_Logs");
		requestBody = RequestBody.req_Patch_tc();
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Patch_API();
    }
	@Given("Enter Name and Job in request body for Patch")
	public void enter_name_and_job_in_request_body_for_patch() throws IOException {
		dir_name = Utility.CreateLogDirectory("Patch_API_Logs");
		requestBody = RequestBody.req_Patch_tc();
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Patch_API();
		response = API_Trigger.Patch_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Patch_TC2"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send  request with payload to the Endpoint for Patch")
	public void send_request_with_payload_to_the_endpoint_for_patch() {
	    // Write code here that turns the phrase above into concrete actions
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0,11);
		
	    //throw new io.cucumber.java.PendingException();
	}

	@Then("validate status code for Patch")
	public void validate_status_code_for_patch() {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(response.statusCode(), 200);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("validate response body parametres for Patch")
	public void validate_response_body_parametres_for_patch() {
		// Write code here that turns the phrase above into concrete actions
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Generate expected date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0,11);

		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
		
		//throw new io.cucumber.java.PendingException();
	}

}
