package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Method.API_Trigger;
import Common_Method.Utility;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PutStepDefinitions {
	File dir_name;
	Response response;
	String Endpoint;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;

	@Before("@Put_API_TC")
    public void beforeScenarioput() throws IOException{
		dir_name = Utility.CreateLogDirectory("Put_API_Logs");
		requestBody = RequestBody.req_put_tc();
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Put_API();
    }
	@Given("Name and Job in Request Body")
	public void name_and_job_in_request_body() throws IOException {
		// Write code here that turns the phrase above into concrete actions
		dir_name= Utility.CreateLogDirectory("PUT_API_Logs");
		requestBody = RequestBody.req_put_tc();
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Put_API();
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Put_TC2"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), Endpoint);

		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Request payload")
	public void send_the_request_payload() {
		// Write code here that turns the phrase above into concrete actions
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate StatusCode")
	public void validate_status_code() {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(response.statusCode(), 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate ResponseBody Parameters")
	public void validate_response_body_parameters() {
		// Write code here that turns the phrase above into concrete actions
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Generate expected date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

		// throw new io.cucumber.java.PendingException();
	}
}
