package stepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Method.API_Trigger;
import Common_Method.Utility;
import Repository.RequestBody;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetStepDefinition {
	String Endpoint;
	File dir_name;
	Response response;
	int status_code3;
	int count;
	String res_body;
	int ids[] = { 7, 8, 9, 10, 11, 12 };
	String Emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
	String firstNames[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
	String lastNames[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

	int res_id;
	String res_email;
	String res_firstName;
	String res_lastName;

	int i;
	
	@Before("@Patch_API_TC")
    public void beforeScenarioget() throws IOException{
		dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Get_API_ListUser();
    }
	@Given("configure endpoint for Get API")
	public void configure_endpoint_for_get_api() throws IOException {
	    // Write code here that turns the phrase above into concrete actions
		dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_Get_API_ListUser();
		response = API_Trigger.Get_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), res_body, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Get_TC1"), dir_name, Endpoint, res_body,
				response.getHeader("Date"), response.getBody().asString());

	    //throw new io.cucumber.java.PendingException();
	}
	@When("send the request to the endpoint of Get API")
	public void send_the_request_to_the_endpoint_of_get_api() {
	    // Write code here that turns the phrase above into concrete actions
		res_body = response.getBody().asPrettyString();
		System.out.println(response.getBody().asPrettyString());
		status_code3 = response.getStatusCode();
		System.out.println("status code is:" + " " + status_code3);
		JsonPath res_jsn = new JsonPath(res_body);
		count = res_jsn.getInt("data.size()");
		System.out.println("count of data array from response:" + " " + count);

		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

		// fetch data and store into newly created array
		for (int i = 0; i < count; i++) {

			System.out.println("\n" + "---------fetched array data from arrays of expected data ---------");

			res_id = res_jsn.getInt("data[" + i + "].id");
			System.out.println("\n" + res_id);
			idArr[i] = res_id;

			res_email = res_jsn.get("data[" + i + "].email");
			System.out.println(res_email);
			emailArr[i] = res_email;

			res_firstName = res_jsn.getString("data[" + i + "].first_name");
			System.out.println(res_firstName);
			fnamesArr[i] = res_firstName;

			res_lastName = res_jsn.getString("data[" + i + "].last_name");
			System.out.println(res_lastName);
			lnamesArr[i] = res_lastName;
		}
	    //throw new io.cucumber.java.PendingException();
	}
	
	@Then("validate status code for Get API")
	public void validate_status_code_for_get_api() {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertEquals(status_code3, 200);
	  //  throw new io.cucumber.java.PendingException();
	}
	
	@Then("validate response body parameters for Get API")
	public void validate_response_body_parameters_for_get_api() {
	    // Write code here that turns the phrase above into concrete actions
		JsonPath res_jsn = new JsonPath(res_body);
		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];
//					create arrays to store fetched data from declared array	

		for (int i = 0; i < count; i++) {

			res_id = res_jsn.getInt("data[" + i + "].id");
			idArr[i] = res_id;

			res_email = res_jsn.getString("data[" + i + "].email");
			emailArr[i] = res_email;

			res_firstName = res_jsn.getString("data[" + i + "].first_name");
			fnamesArr[i] = res_firstName;

			res_lastName = res_jsn.getString("data[" + i + "].last_name");
			lnamesArr[i] = res_lastName;

			// validate response body parameters

			Assert.assertEquals(ids[i], idArr[i]);
			Assert.assertEquals(Emails[i], emailArr[i]);
			Assert.assertEquals(firstNames[i], fnamesArr[i]);
			Assert.assertEquals(lastNames[i], lnamesArr[i]);
		}
	    //throw new io.cucumber.java.PendingException();
	}
}
