package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = { "stepDefinitions" },tags="@DataDriven or @Post_API_TC or @Get_API_TC or @Patch_API_TC or @Put_API_TC ")
public class TestRunner {

}