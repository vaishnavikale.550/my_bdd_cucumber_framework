Feature:  Trigger the PUT API and Validate the Response Body and Response Parameters

@Put_API_TC
Scenario: Trigger the API request with valid request parameters
       Given Name and Job in Request Body
       When Send the Request payload 
       Then Validate StatusCode
       And Validate ResponseBody Parameters