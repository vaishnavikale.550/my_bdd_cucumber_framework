Feature: Trigger the patch API and validate response parametres

@Patch_API_TC
Scenario: Trigger the Api request with valid request body parametres for Patch API
          Given Enter Name and Job in request body for Patch
          When Send  request with payload to the Endpoint for Patch
          Then validate status code for Patch
          And validate response body parametres for Patch
