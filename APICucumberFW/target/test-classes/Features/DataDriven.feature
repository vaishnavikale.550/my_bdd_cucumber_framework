Feature: Trigger Post API on the basis of Input Data

@DataDriven
Scenario Outline: Trigger the Post API request with valid request parameters
       Given "<Name>" and "<Job>" in Request Body
       When Send the Request payload to the Endpoint
       Then Validate Status Code
       And Validate Response Body Parameters
       
Examples:

        |Name|Job|
        |Vaishnavi|Engg|
        |Sneha|graphics|