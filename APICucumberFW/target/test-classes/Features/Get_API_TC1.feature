Feature: Trigger the Get APi and validate response parametres

@Get_API_TC
Scenario: Trigger the Get API request with valid string request body parameters
         Given configure endpoint for Get API
         When send the request to the endpoint of Get API
         Then validate status code for Get API
         And validate response body parameters for Get API
