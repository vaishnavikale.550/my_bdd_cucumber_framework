Feature:  Trigger the Post API and Validate the Response Body and Response Parameters

@Post_API_TC
Scenario: Trigger the API request with valid request parameters
       Given Enter Name and Job in Request Body
       When Send the Request payload to the Endpoint
       Then Validate Status Code
       And Validate Response Body Parameters